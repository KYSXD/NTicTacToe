ALL_FILES := $(sort $(wildcard $(SRC_FOLDER)*/*.h) $(wildcard $(SRC_FOLDER)*/*.cpp))

PIPFILE := Pipfile
PIPFILE_LOCK := Pipfile.lock

.PHONY: default_target
default_target: help

$(PIPFILE_LOCK): $(PIPFILE)
	@pipenv install --dev

.PHONY: lint
lint: $(PIPFILE_LOCK)
	@pipenv run cpplint --filter=-build/include_subdir $(ALL_FILES)

.PHONY: help
help:
	@echo " lint"
	@echo "     Runs the linter"

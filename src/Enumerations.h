#pragma once

namespace OWNER
{
    enum ENUM
    {
        NONE    = 0,
        O       = 1,
        X       = 2,
        TIE     = 3,
    };
};

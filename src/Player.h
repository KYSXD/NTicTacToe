#pragma once

#include "Enumerations.h"
#include <vector> //std::vector
#include <utility> //std::pair

#include "NTicTacToe.h"

// Abstract class
class BasePlayer
{
public:
    virtual int playerMove(int lastMove, std::vector<int> *game) = 0;

    const OWNER::ENUM getId() const { return m_id; }

protected:
    explicit BasePlayer(OWNER::ENUM id) : m_id(id) {}
    virtual ~BasePlayer() {}

protected:
	OWNER::ENUM m_id;
};

#pragma once

#include "NTicTacToe.h"
#include "Player.h"

#include "BasePlayer.h"

#include <vector>

class Game
{
private:
	std::vector<BasePlayer*> *p_players;
	NTTT *p_gameBoard;

	bool m_bPlaying;

public:
	Game(BasePlayer *player1, BasePlayer *player2) :
		m_bPlaying(true;)
	{
		p_players = new std::vector<BasePlayer*>;

		p_players->push_back(player1);
		p_players->push_back(player2);
		return;
	}

	~Game()
	{
		delete p_players;
	}

	bool play()
	{

		unsigned currentId = 0;

		int lastMove = -1;
		int nextMove = -1;

		while(m_bPlaying)
		{
			BasePlayer *currentPlayer = p_players->at(currentId);
			nextMove = currentPlayer->playerMove(lastMove, p_gameBoard->getAsVector());

			if(isValidMove(nextMove, lastMove))
			{
				p_gameBoard->setMove(nextMove, currentPlayer->getId());

				lastMove = nextMove;
			}

			currentId = (currentId+1)%2;

			m_bPlaying = (p_gameBoard->getOwner() != OWNER::NONE);
		}
	}
};





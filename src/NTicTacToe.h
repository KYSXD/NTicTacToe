#pragma once

#include "Enumerations.h"

#include <vector> //std::vector

class NTTT
{
private:
    unsigned int            m_dimension;
    std::vector<NTTT*>      *p_board;
    OWNER::ENUM             m_owner;

public:
	/* KYSXD:
	 * Common utilities (element access)
	 */
    NTTT* operator()( unsigned i)
    {
        NTTT *board = NULL;
        if(p_board != NULL)
        {
            board = p_board->at(i);
        }
        return board;
    }

    const NTTT* operator()( unsigned i) const
    {
        NTTT *board = NULL;
        if(p_board != NULL)
        {
            board = p_board->at(i);
        }
        return board;
    }

    NTTT* operator()( unsigned i, unsigned j )
    {
        NTTT *board = NULL;
        if(p_board != NULL)
        {
            board = p_board->at(i*3 + j);
        }
        return board;
    }

    const NTTT* operator()( unsigned i, unsigned j ) const
    {
        NTTT *board = NULL;
        if(p_board != NULL)
        {
            board = p_board->at(i*3 + j);
        }
        return board;
    }

    NTTT* element(unsigned i, unsigned j)
    {
        NTTT *board = NULL;
        if(p_board != NULL)
        {
            board = p_board->at(i*3 + j);
        }
        return board;
    }

    const NTTT* element(unsigned i, unsigned j) const
    {
        NTTT *board = NULL;
        if(p_board != NULL)
        {
            board = p_board->at(i*3 + j);
        }
        return board;
    }

    NTTT* element(unsigned i)
    {
        NTTT *board = NULL;
        if(p_board != NULL)
        {
            board = p_board->at(i);
        }
        return board;
    }

    const NTTT* element(unsigned i) const
    {
        NTTT *board = NULL;
        if(p_board != NULL)
        {
            board = p_board->at(i);
        }
        return board;
    }

    NTTT* element(std::pair<int, int> p)
    {
    	return
    		element(p.first, p.second);
    }

    const NTTT* element(std::pair<int, int> p) const
    {
    	return
    		element(p.first, p.second);
    }

    NTTT* operator()(std::pair<int, int> p)
    {
        return
            element(p.first, p.second);
    }

    const NTTT* operator()(std::pair<int, int> p) const
    {
        return
            element(p.first, p.second);
    }

public:
    NTTT(int levels);

    ~NTTT();

    /* KYSXD:
     * Victory check
     */
    bool findColVictory();
    bool findRowVictory();
    bool findDiagonalVictory();
    bool findTie();

    /* KYSXD:
     * Update methods
     */
    void updateOwner();

    /* KYSXD:
     * Info
     */
    const OWNER::ENUM getOwner() { return m_owner;}
    const int getDimension() { return m_dimension;}

    /* KYSXD:
     * Utilities
     */
    template <class T>
    NTTT* getCellUtil(std::vector<T> *coords, int pos,
        bool checkPlayable = false);
    template <class T>
    NTTT* getCell(std::vector<T> *coords);
    NTTT* getCell(int cell);

    /* KYSXD:
     * Modifiers
     */
    template <class T>
    bool setMoveUtil(std::vector<T> *coords,
        int pos, OWNER::ENUM owner);
    template <class T>
    bool setMove(std::vector<T> *coords, OWNER::ENUM owner);
    bool setMove(int pos, OWNER::ENUM owner);

    const std::vector<int> getAsVector();
};

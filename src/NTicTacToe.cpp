#include "NTicTacToe.h"
#include "Helpers.h"

#include <iostream>

#include <cmath> //pow

NTTT::NTTT(int levels) :
    m_dimension(levels),
    p_board(NULL),
    m_owner(OWNER::NONE)
{
    if(levels)
    {
        p_board = new std::vector<NTTT*>;
        for(int i = 0; i < 9; i++)
        {
            p_board->push_back(new NTTT(levels-1));
        }
    }
    return;
};

NTTT::~NTTT()
{
    for(std::vector<NTTT*>::iterator it = p_board->begin();
        it != p_board->end();
        it++)
    {
        if((*it) != NULL)
        {
            delete (*it)->p_board;
        }
    }
    delete p_board;
    return;
};

bool NTTT::findColVictory()
{
    bool bWinner = false;
    for(int i = 0; i < 3; i++)
    {
        if(element(0, i)->m_owner != OWNER::NONE
            && element(0, i)->m_owner != OWNER::TIE
            && element(0, i)->m_owner == element(1, i)->m_owner
            && element(1, i)->m_owner == element(2, i)->m_owner)
        {
            m_owner = element(0, i)->m_owner;
            bWinner = true;
        }
    }
    return bWinner;
};

bool NTTT::findRowVictory()
{
    bool bWinner = false;
    for(int i = 0; i < 3; i++)
    {
        if(element(i, 0)->m_owner != OWNER::NONE
            && element(i, 0)->m_owner != OWNER::TIE
            && element(i, 0)->m_owner == element(i, 1)->m_owner
            && element(i, 1)->m_owner == element(i, 2)->m_owner)
        {
            m_owner = element(i, 0)->m_owner;
            bWinner = true;
        }
    }
    return bWinner;
};

bool NTTT::findDiagonalVictory()
{
    bool bWinner = false;
    if(element(0, 0)->m_owner != OWNER::NONE
        && element(0, 0)->m_owner != OWNER::TIE
        && element(0, 0)->m_owner == element(1, 1)->m_owner
        && element(1, 1)->m_owner == element(2, 2)->m_owner)
    {
        m_owner = element(0, 0)->m_owner;
        bWinner = true;
    }

    else if(element(0, 2)->m_owner != OWNER::NONE
        && element(0, 2)->m_owner != OWNER::TIE
        && element(0, 2)->m_owner == element(1, 1)->m_owner
        && element(1, 1)->m_owner == element(2, 0)->m_owner)
    {
        m_owner = element(0, 2)->m_owner;
        bWinner = true;
    }

    return bWinner;
}

bool NTTT::findTie()
{
    bool bTie = true;
    for(std::vector<NTTT*>::iterator it = p_board->begin();
        it != p_board->end() && bTie;
        it++)
    {
        if((*it)->m_owner == OWNER::NONE)
        {
            bTie = false;
        }
    }
    if(bTie)
	{
		m_owner = OWNER::TIE;
	}
    return bTie;
}

void NTTT::updateOwner()
{
    bool bContinue = (p_board != NULL);
    if(bContinue)
    {
        bContinue = !findColVictory();
    }
    if(bContinue)
    {
        bContinue = !findRowVictory();
    }
    if(bContinue)
    {
        bContinue = !findDiagonalVictory();
    }
    if(bContinue)
    {
        findTie();
    }
    return;
}

template <class T>
NTTT* NTTT::getCellUtil(std::vector<T> *coords, int pos,
    bool checkPlayable)
{
    NTTT *auxBoard = (this);

    for(int i = 0;
        auxBoard
        && i < coords->size()
        && i < pos;
        i++)
    {
        auxBoard = auxBoard->element(coords->at(i));
        if(checkPlayable
            && auxBoard->getOwner() != OWNER::NONE)
        {
            auxBoard = NULL;
        }
    }
    return auxBoard;
};

template <class T>
NTTT* NTTT::getCell(std::vector<T> *coords)
{
    NTTT *cell = NULL;
    if(m_dimension >= coords->size())
    {
        cell = getCellUtil(coords, (int)coords->size());
    }
    return cell;
}

NTTT* NTTT::getCell(int cell)
{
    std::vector< std::pair<int, int> > intAsPair =
        HELPERS::cellToPairVector(cell, m_dimension);
    return getCell(&intAsPair);
}

template <class T>
bool NTTT::setMoveUtil(std::vector<T> *coords,
    int pos, OWNER::ENUM owner)
{
    bool bMove = false;
    if(m_owner == OWNER::NONE)
    {
        NTTT *game = getCellUtil(coords, (int)coords->size(), true);

        if(game != NULL)
        {
            if(game->m_owner == OWNER::NONE)
            {
                game->m_owner = owner;
	                bMove = true;
            }
        }

        for(unsigned i = 0; i < coords->size(); i++)
        {
            NTTT* auxGame = getCellUtil(coords, (int)coords->size() - i);
            if(auxGame)
            {
                auxGame->updateOwner();
            }
        }

        updateOwner();
    }
    return bMove;
};

template <class T>
bool NTTT::setMove(std::vector<T> *coords, OWNER::ENUM owner)
{
    bool bMove = false;
    if(coords->size() == m_dimension)
    {
        bMove = setMoveUtil(coords, 0, owner);
    }
    return bMove;
}

bool NTTT::setMove(int pos, OWNER::ENUM owner)
{
    bool bMove = false;
    if(pos < pow(9, m_dimension))
    {
        std::vector< std::pair<int, int> > intAsPair
            = HELPERS::cellToPairVector(pos, m_dimension);
        bMove = setMove(&intAsPair, owner);
    }
    return bMove;
}

const std::vector<int> NTTT::getAsVector()
{
    std::vector<int> resVector( pow(9, m_dimension), 0);

    for(int i = 0; i < resVector.size(); i++)
    {
        std::vector< std::pair<int, int> > cellAsPairVector =
            HELPERS::cellToPairVector(i, m_dimension);

        NTTT* currentCell = getCell(&cellAsPairVector);
        resVector.at(i) = currentCell->m_owner;
    }

    return resVector;
}

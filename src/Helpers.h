#pragma once

#include <vector> //std::vector
#include <utility> //std::pair

#include "NTicTacToe.h"

namespace HELPERS
{
	/* KYSXD:
	 * Returns an integer as a vector in a specific base
	 */
	std::vector<int> intToBase(int value, int base);

	/* KYSXD:
	 * Returns a cell's coord as an integer
	 */
	unsigned pairVectorToCell(std::vector< std::pair<int, int> > *pairs);

	/* KYSXD:
	 * Returns a cell id as a vector of pairs
	 */
	std::vector< std::pair<int, int> > cellToPairVector(int cell, int lenght);

	/* KYSXD:
	 * Returns a vector of ints as a vector of pairs
	 */
	std::vector<int> pairVectorToIntVector(std::vector< std::pair<int, int> > *pairs);

	/* KYSXD:
	 * Returns a vector of pairs as a vector of ints
	 */
    std::vector< std::pair<int, int> > intVectorToPairVector(std::vector<int> *ints);

	/* KYSXD:
	 * Returns a cell (0 - 8 value) as tuple
	 */
	int pairToCell(std::pair<int, int> *pair);

	/* KYSXD:
	 * Returns a tuple (a, b) mod 3 from a cell id (0 - 9 value)
	 */
	std::pair<int, int> cellToPair(int cell);

	/* KYSXD:
	 * Prints a vector of pairs
	 */
	void coutPairVector(std::vector< std::pair<int, int> > *pairs);

	/* KYSXD:
	 * Prints a vector of integers
	 */
	void coutIntVector(std::vector< int > *pairs);

	/* KYSXD:
	 * Prints a game class in console (via std::cout)
	 */
	void coutGame(NTTT *game);
};

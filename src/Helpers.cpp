#include "Helpers.h"

#include <cmath> //pow
#include <iostream> //std::cout, std::cin, std::endl

std::vector<int> HELPERS::intToBase(int value, int base)
{
	std::vector<int> resVector;
	int remainder;

	while(value)
	{
		remainder = value%base;
		value /= base;

		resVector.push_back(remainder);
	}

	return resVector;
}

unsigned HELPERS::pairVectorToCell(std::vector< std::pair<int, int> > *pairs)
{
	unsigned res = 0;
	unsigned size = (unsigned)pairs->size();

	for(unsigned i = 0; i < size; i++)
	{
		res += pairs->at(i).first * pow(3, 2*size - (i+1));
		res += pairs->at(i).second * pow(3, size - (i+1));
	}

	return res;
}

std::vector< std::pair<int, int> > HELPERS::cellToPairVector(int cell, int lenght)
{
	std::vector< std::pair<int, int> > resVector(lenght, std::pair<int, int>(0,0) );

	std::vector<int> base3 = intToBase(cell, 3);

	for(unsigned i = 0; i < base3.size() && i < lenght; i++)
	{
		resVector.at((lenght - 1) - i).second = base3.at(i);

		if(lenght + i  < base3.size())
		{
			resVector.at((lenght - 1) - i).first = base3.at(lenght + i);
		}
	}

	return resVector;
}

std::vector<int> HELPERS::pairVectorToIntVector(std::vector< std::pair<int, int> > *pairs)
{
	std::vector<int> resVector;
	for(std::vector< std::pair<int, int> >::iterator it = pairs->begin();
		it != pairs->end();
		it++)
	{
        resVector.push_back( HELPERS::pairToCell(&(*it)) );
	}
	return resVector;
}

std::vector< std::pair<int, int> > HELPERS::intVectorToPairVector(std::vector<int> *ints)
{
	std::vector< std::pair<int, int> > resVector;
	for(std::vector<int>::iterator it = ints->begin();
		it != ints->end();
		it++)
	{
        resVector.push_back( HELPERS::cellToPair(*it) );
	}
	return resVector;
}

int HELPERS::pairToCell(std::pair<int, int> *pair)
{
	return pair->first*3 + pair->second;
}

std::pair<int, int> HELPERS::cellToPair(int cell)
{
	std::pair<int, int> pair;
	std::vector<int> base3 = intToBase(cell, 3);

	pair.first = base3.at(1);
	pair.second = base3.at(0);
	return pair;
}

void HELPERS::coutPairVector(std::vector< std::pair<int, int> > *pairs)
{
	for(std::vector< std::pair<int, int> >::iterator it = pairs->begin(); it != pairs->end(); it++)
	{
		std::cout << "(" << (*it).first << "," << (*it).second << ") ";
	}
	std::cout << std::endl;
}

void HELPERS::coutIntVector(std::vector< int > *pairs)
{
	for(std::vector< int >::iterator it = pairs->begin(); it != pairs->end(); it++)
	{
		std::cout << "(" << (*it) << ")\n";
	}
	std::cout << std::endl;
}

void HELPERS::coutGame(NTTT *game)
{
    std::vector<int> gameVector = game->getAsVector();

    for(int i = 0; i < gameVector.size(); i++)
    {
        NTTT* currentCell = game->getCell(i);
        if(currentCell != NULL)
        {
            std::cout << currentCell->getOwner();
        }
        else
        {
            std::cout << "X";
        }

        if( (i+1) % 3 == 0 )
        {
            std::cout << "+";
        }

        if( (i+1) % ( (int) pow(3, game->getDimension()) ) == 0)
        {
            std::cout << std::endl;
        }

        if( (i+1) % ( (int)pow(3, game->getDimension())*3 ) == 0)
        {
            for(int j = 0; j < (int)pow(3, game->getDimension()) + pow(3, game->getDimension()-1); j++)
            {
                std::cout <<  "+";
            }
            std::cout << std::endl;
        }

    }

    return;
}


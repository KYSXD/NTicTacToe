#include "HumanPlayer.h"

#include "Helpers.h"

#include <iostream>

HumanPlayer::HumanPlayer(OWNER::ENUM id) : BasePlayer(id) {}

int HumanPlayer::playerMove(int lastMove, std::vector<int> *game)
{
    int cellMove = -1;

    std::cout << "Enter move\n";
    std::cin >> cellMove;

    return cellMove;
}

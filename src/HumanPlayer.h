#pragma once

#include "Player.h"

#include "NTicTacToe.h"

class HumanPlayer : public BasePlayer
{
public:
	HumanPlayer(OWNER::ENUM id);
    virtual ~HumanPlayer() {}

    virtual bool playerMove(int lastMove, std::vector<int> *game);
};
